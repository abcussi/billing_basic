BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "clients" (
	"id"	INTEGER NOT NULL UNIQUE,
	"name"	TEXT,
	"payment"	INTEGER,
	PRIMARY KEY("id"),
	FOREIGN KEY("payment") REFERENCES "payments"("id") ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE IF NOT EXISTS "payments" (
	"id"	INTEGER NOT NULL UNIQUE,
	"category"	TEXT,
	"period"	TEXT,
	"amount"	INTEGER,
	"paymentStatus"	INTEGER,
	PRIMARY KEY("id"),
	FOREIGN KEY("paymentStatus") REFERENCES "payment-statuses"("id") ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE IF NOT EXISTS "payment-statuses" (
	"id"	INTEGER NOT NULL UNIQUE,
	"type"	TEXT,
	PRIMARY KEY("id")
);
CREATE INDEX IF NOT EXISTS "category" ON "payments" (
	"category"	DESC
);
PRAGMA foreign_keys = ON;
COMMIT;
